﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*
 * Will control when the methods in ChangeForgroundColors, ChangeBackgroundColors, and InvisibleController will be called.
 * This script is meant to be attached to the cursed torch.
 */
public class CurseOfSightController : MonoBehaviour {

    public bool near_player = false;        //Whether or not palyer is within range of torch
    public float time_limit = 15f;          //How long the curse will last for
    public float start_time = 0f;           //Beginning of timer
    public float time_left;                 //time_limit - start_time
    public bool activated = false;
    public LayerMask player;
    public ChangeForegroundColors ChangeForeColors;
    public ChangeBackgroundColors ChangeBackColors;
    public InvisibleController ChangeVisiblity;
    public Text timer_text;                 //display of time left before curse expires
    public Image eye;                       //Icon to indicate player is cursed
    public AudioSource contact;             //Sound to play when passing torch
    public AudioSource activation;          //Sound to play when activating curse
	
	void Update () {
		near_player = gameObject.GetComponent<Collider2D>().IsTouchingLayers(player);   //Continuously checks if the player is within range of the torch

        if (near_player)    //plays the sound when passing by
            contact.Play();

        //After being activated, Will increment start_time and subtract from time_limit to get how much time is left
        //It then updates the text on the UI
        if (activated)
        {
            start_time += Time.deltaTime;
            time_left = time_limit - start_time + 1;
            timer_text.text = ((int)time_left).ToString();
            if (start_time >= time_limit)
            {
                ChangeForeColors.ToggleCursedState();
                ChangeBackColors.ToggleCursedState();
                ChangeVisiblity.ToggleVisibility();
                activated = false;
                start_time = 0f;
                eye.enabled = activated;
                timer_text.enabled = activated;
            }
        }

        //When initially being activated, plays the sound, resets start_time, enables both the UI elements
        //and calls the relevant methods to change the scenery.
        if (near_player && Input.GetKeyDown(KeyCode.X))
        {
            activation.Play();
            start_time = 0f;
            time_left = time_limit - start_time + 1f;

            //If activating again before curse expires, only refresh the timer and skip this
            if (!activated)
            {
                eye.enabled = !activated;
                timer_text.text = time_left.ToString();
                timer_text.enabled = !activated;
                ChangeForeColors.ToggleCursedState();
                ChangeBackColors.ToggleCursedState();
                ChangeVisiblity.ToggleVisibility();
            }
            activated = true;
        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Used for getting our credits to roll in front of the camera from top to bottom
public class RollCredits : MonoBehaviour {

    public Text end_credits;                //The credits
    public float max_time = 142f;           //The length of the end credits music
    public float start_time = 0f;           //Acts as our accumulated time
    public float starting_transform_y;      //Gets the original position of our end credits
    public float ending_transform_y = 1777; //Where i want the credits position to end up
    public float pixels_per_second;         //distance divided by amount of time
    public Game_Manager manager;
	
    //Calculates our speed for the rolling the credits
	void Start () {
        starting_transform_y = end_credits.rectTransform.position.y;
        pixels_per_second = (ending_transform_y - starting_transform_y) / max_time;
	}
	
	//Sets the transorm of our credits based on how far we are into the song
	void Update () {
        end_credits.transform.position = new Vector3(end_credits.transform.position.x, (start_time * pixels_per_second) + starting_transform_y);
        start_time += Time.deltaTime;
        if (start_time >= max_time)
            manager.Loadlevel(0);
    }
}

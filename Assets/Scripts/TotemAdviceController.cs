﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Used by the advice totems, to give small hints on what to do next
public class TotemAdviceController : MonoBehaviour {

    public bool near_player = false;
    public LayerMask player;
    public Text totem_message;      //Message to be displayed on the UI
    public Image dimming_panel;     //Dims the game while displaying the message
    public int totem_number;        //Which totem are we activating
    public static string[] totem_messages = new string[10]; //Array of messages
    public SpriteRenderer[] sprites;    //access the renderer of the arrow above the totem
    public AudioSource contact;         //Sound that is played when passing by the totem

    // Use this for initialization
    void Start () {
        sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();
	    totem_messages[0] = "Congratulations, Adventurer.\n\nKudos on getting passed the first trap.\n(or did you...)" +
                                    "\n\nBut YOU!! YOU.... are one of a kind.\n\n\nCome, show me your nimble skills.";
        totem_messages[1] = "Impressive.\n\nNot only can you climb, but you can swim too.\n\nI'm curious, can you swim through lava as well?";
        totem_messages[2] = "Such faith.\n\nA reward is in order....\n\n\nTry touching that flame over there and SEE if you can make it to the door.";
    }
	
	// Update is called once per frame
	void Update () {
        //Checks if the player is within range to activate the totem
        near_player = gameObject.GetComponent<Collider2D>().IsTouchingLayers(player);

        //Displays an arrow icon to inform the player that it is activateable
        if (near_player)
        {
            //Makes the arrow rotate
            sprites[1].enabled = true;
            Vector3 orientation = sprites[1].gameObject.transform.localScale;
            orientation.x -= Time.deltaTime;
            contact.Play();
            if (orientation.x < -1)
            {
                orientation.x = 1;

            }
            sprites[1].gameObject.transform.localScale = orientation;
        }

        //hides the arrow if not in range
        else
        {
            sprites[1].enabled = false;
            Vector3 orientation = sprites[1].gameObject.transform.localScale;
            orientation.x = 1;
            sprites[1].gameObject.transform.localScale = orientation;
        }

        //If in range and presses 'X' then the game is paused and you can read the totem's message
        if (near_player && Input.GetKeyDown(KeyCode.X))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                totem_message.text = totem_messages[totem_number];
                dimming_panel.enabled = true;
                totem_message.enabled = true;
            }
            else if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                totem_message.enabled = false;
                dimming_panel.enabled = false;
            }
        }
    }

}

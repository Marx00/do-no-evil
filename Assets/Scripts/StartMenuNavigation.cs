﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Used for navigating the buttons that appear in the start menu
public class StartMenuNavigation : MonoBehaviour {

    public Button[] buttons;    //Our array of buttons
    public Button back;         //Back button
    public Text title;          //Text that has our title
    public Text control_instructions;   //Text that shows the controls for the game
    public bool in_controls_menu = false;   //Whether or not we are looking at the controls text

    //Initially, have all buttons and the title visible
	void Start () {
        foreach (Button button in buttons)
        {
            button.gameObject.SetActive(true);
        }
        back.gameObject.SetActive(false);
        title.enabled = true;
        control_instructions.enabled = false;
    }

    //When back button is pressed, hide the other buttons and reveal the controls
    public void ToggleMenu()
    {
        in_controls_menu = !in_controls_menu;
        foreach(Button button in buttons)
        {
            button.gameObject.SetActive(!in_controls_menu);
        }
        back.gameObject.SetActive(in_controls_menu);
        title.enabled = !in_controls_menu;
        control_instructions.enabled = in_controls_menu;
    }
}

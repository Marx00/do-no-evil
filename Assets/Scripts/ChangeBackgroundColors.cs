﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Turns the colors of the background to have no color while in isCursed state.
 */
public class ChangeBackgroundColors : MonoBehaviour {

    public SpriteRenderer[] back_sprites;   //Sprites to be changed
    public bool isCursed = false;           //whether or not in cursed state

	void Start () {
        back_sprites = GetComponentsInChildren<SpriteRenderer>();   //Get the SpriteRenderer component for each child
    }                                                               //Meant to target all background tiles
	
    //Method for transitioning between the states.
    public void ToggleCursedState()
    {
        if (isCursed)   //If already cursed, remove the curse and change everything back to normal colors
        {
            foreach (SpriteRenderer sprite in back_sprites)
                sprite.color = new Color(1.0f, 1.0f, 1.0f);
            isCursed = false;
        }
        else if (!isCursed) //If not cursed, become cursed and change the background to all black.
        {
            foreach (SpriteRenderer sprite in back_sprites)
                sprite.color = new Color(0.0f, 0.0f, 0.0f);
            isCursed = true;
        }
    }
}

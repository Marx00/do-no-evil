﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Used for tiles that can kill the player
public class HazardousTerrain : MonoBehaviour {

    public Text game_over;
    public Text how_to_respawn;

    //Kill the player
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(collision.gameObject);
            restart();

        }
    }

    //Reveals game over text and instructions on how to restart
    public void restart()
    {
        game_over.enabled = true;
        how_to_respawn.enabled = true;
    }
}

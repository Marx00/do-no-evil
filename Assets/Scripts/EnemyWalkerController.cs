﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Will hold the 'motor' that will enable the enemy to walk around and kill the player if the collide
public class EnemyWalkerController : MonoBehaviour {

    public bool facing_right = false;
    public float max_speed = 5f;
    public float moving_force = 1;
    public bool first_anim = true;
    public float sprite_timer = 0.5f;
    public float start_time = 0f;

    public Sprite anim1;
    public Sprite anim2;
    public Text game_over;
    public Text how_to_respawn;

    void Start () {
        gameObject.GetComponent<SpriteRenderer>().sprite = anim1;   //Start off with best foot forward
    }
	
	//At every 1 second interval, the animation for the enemy is switched to the opposite one
    //Also continuously propels the enemy in the way it is facing until it collides with something and Flip() is called
	void Update () {

        start_time += Time.deltaTime;
        if (start_time >= sprite_timer)
        {
            start_time = 0f;
            if (first_anim)
                gameObject.GetComponent<SpriteRenderer>().sprite = anim2;
            else
                gameObject.GetComponent<SpriteRenderer>().sprite = anim1;
            first_anim = !first_anim;
        }

        if (facing_right)
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moving_force * max_speed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
        else
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-moving_force * max_speed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Make the enemy turn around when it hits an ally or wall
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "wall")
        {
            Flip();
            gameObject.GetComponent<SpriteRenderer>().sprite = anim1;
        }

        //Kill the player
        if (collision.gameObject.tag == "Player")
        {
            Destroy(collision.gameObject);
            restart();

        }
    }
    //Flip the orientation of the enemy
    private void Flip()
    {
        facing_right = !facing_right;
        Vector3 orientation = transform.localScale;
        orientation.x *= -1;
        transform.localScale = orientation;
    }

    //Reveal game over text and instructions on how to restart
    public void restart()
    {
        game_over.enabled = true;
        how_to_respawn.enabled = true;
    }
}

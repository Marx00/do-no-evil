﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controls the visibility of objects that require the curse to see
public class InvisibleController : MonoBehaviour {

    public SpriteRenderer[] invisible_sprites;  //Sprites to be toggled
    public bool isCursed = false;               //Whether or not in cursed state

    void Start () {
        invisible_sprites = GetComponentsInChildren<SpriteRenderer>();  //Gets the SpriteRenderer components from each child
        foreach (SpriteRenderer sprite in invisible_sprites)            //Meant to target stuctures that are supposed to be invisible at first
            sprite.enabled = isCursed;      //turn them invisible
    }

    //If already cursed, remove the curse and hide the structures again
    //If not cursed, become cursed and reveal the hidden structures
    public void ToggleVisibility()
    {
        isCursed = !isCursed;
        foreach (SpriteRenderer sprite in invisible_sprites)
        {
            sprite.enabled = isCursed;
        }
    }
}

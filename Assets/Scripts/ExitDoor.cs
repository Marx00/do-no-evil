﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Used by the door at the end of the ruins_dungeion scene. Takes you to the end_credits scene
public class ExitDoor : MonoBehaviour {

    public Collider2D player;       //The players collider
    public int next_scene;          //Which scene to go to next
    public int number_of_scenes = 3;//Total of 3 scenes in the game
    public Game_Manager manager;    //Singleton game manager to load scenes

	// Determine what the next scene is supposed to be
	void Start () {
        next_scene = (SceneManager.GetActiveScene().buildIndex + 1) % number_of_scenes;
	}
	
	// Continuously checks if the player is within range to use the door.
	void Update () {
		if (gameObject.GetComponent<Collider2D>().IsTouching(player) && Input.GetKeyDown(KeyCode.X))
        {
            manager.Loadlevel(next_scene);
        }
	}
}

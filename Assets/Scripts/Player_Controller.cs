﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Will act as the 'motor' for allowing the player to control the character
/* HOW DID I HANDLE WALL JUMPING:
 *  I ended up using a charge system when determining if the character was allowed to jump.
 *  In order to get extra jumps while not touching the ground, the character has to be touching
 *  a 'new' wall. Otherwise they will not get an extra jump. This prevents the character from
 *  spamming the same wall and scaling it that way. This forces the character to go from one wall to
 *  the other in order to go up chasms.
 */
public class Player_Controller : MonoBehaviour {

    public float max_speed = 7f;
    public bool facing_right = true;
    public bool on_wall = false;        //Whether or not the character is attached to a wall
    public bool on_ground = false;      //Whether or not the character is on the ground
    public int extra_jumps = 0;         //Extra jumps act as an ammo system from wall jumping
    public bool in_water = false;       //Whether or not the character is under water

    public LayerMask water;

    public float jumping_force = 300f;
    public float swimming_force = 10f;  //Weaker jump when trying to jump in water, compensates for having "unlimited" jumps while in water

    GameObject previous_wall = null;    //A reference to what is supposed to be the previous wall touched

    //Continuously checks if the character is allowed to jump, whether through already touching the ground or from being attached to a new wall
    private void Update()
    {
        if ((extra_jumps>0 || on_ground) && Input.GetKeyDown(KeyCode.Space))
        {
            if (extra_jumps > 0)
                extra_jumps--;
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumping_force));
        }

    }

    //Handles movement of the character. Mostly inspired by "2D character controllers" tutorial on unity website
    void FixedUpdate () {

        float move = Input.GetAxis("Horizontal");
        in_water = gameObject.GetComponent<Collider2D>().IsTouchingLayers(water);

        //If in water, have 'unlimited' mini jumps.
        if (in_water && Input.GetKeyDown(KeyCode.Space))
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, swimming_force));

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(move * max_speed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
        if (facing_right && move < 0)
        {
            Flip();
        }

        else if (!facing_right && move > 0)
        {
            Flip();
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        //You are allowed to jump and previous wall is cleared
        if (collision.gameObject.tag == "ground")
        {
            on_ground = true;
            previous_wall = null;
        }

        //You are allowed to jump from this wall if it is not the same as the last one you touched
        if ((collision.gameObject.tag == "wall") && (previous_wall != collision.gameObject))
        {
            extra_jumps++;
            on_wall = true;
            previous_wall = collision.gameObject;


        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        //You are already midair, and cannot jump anymore
        if (collision.gameObject.tag == "ground")
        {
            on_ground = false;
        }

        //Remove the extra jump, when leaving the wall
        if (collision.gameObject.tag == "wall")
        {
            on_wall = false;
            if (on_ground)
                extra_jumps--;
        }
    }

    //Flip the orientation of our character
    void Flip()
    {
        facing_right = !facing_right;
        Vector3 orientation = transform.localScale;
        orientation.x *= -1;
        transform.localScale = orientation;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Will hold the 'motor' that will enable the enemy to jump around and kill the player if they collide
public class EnemyJumperController : MonoBehaviour {

    public float jumping_force = 400f;
    public float horizontal_force = 300f;
    public bool facing_right = false;

    public Sprite anim1;
    public Sprite anim2;
    public Text game_over;
    public Text how_to_respawn;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Jumps again when it hits the ground
        if (collision.gameObject.tag == "ground")
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = anim1;
            if (facing_right)
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(horizontal_force, jumping_force));
            else
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-horizontal_force, jumping_force));
        }

        //Similar to "ground" case except with a weaker upward jump
        if (collision.gameObject.tag == "Enemy")
        {
            Flip();
            gameObject.GetComponent<SpriteRenderer>().sprite = anim1;
            if (facing_right)
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(horizontal_force, 200));
            else
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-horizontal_force, 200));
        }

        //Attempts to move away from the wall at the same force it was moving with before collision
        if (collision.gameObject.tag == "wall")
        {
            Flip();
            if (facing_right)
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(horizontal_force, 0));
            else
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-horizontal_force, 0));
        }

        //Kill the player
        if (collision.gameObject.tag == "Player")
        {
            Destroy(collision.gameObject);
            restart();

        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //In both cases, simply enter jumping stance
        if (collision.gameObject.tag == "ground")
            gameObject.GetComponent<SpriteRenderer>().sprite = anim2;

        if (collision.gameObject.tag == "Enemy")
            gameObject.GetComponent<SpriteRenderer>().sprite = anim2;
    }

    //Flip the orientation of the enemy
    private void Flip()
    {
        facing_right = !facing_right;
        Vector3 orientation = transform.localScale;
        orientation.x *= -1;
        transform.localScale = orientation;
    }

    //Reveal game over text and instructions on how to restart
    public void restart()
    {
        game_over.enabled = true;
        how_to_respawn.enabled = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Turns the colors of the foreground to have altered colors while in isCursed state.
 */
public class ChangeForegroundColors : MonoBehaviour {

    public SpriteRenderer[] fore_sprites;   //Sprites to be changed
    public bool isCursed = false;           //Whether or not in cursed state

	// Use this for initialization
	void Start () {
        fore_sprites = GetComponentsInChildren<SpriteRenderer>();   //Gets the SpriteRenderer components from each of its children
    }                                                               //Meant to target all of the foreground tiles

    //Method for transitioning between the states
    public void ToggleCursedState()
    {
        if (isCursed)   //If already cursed, remove the curse and change all colors back to normal
        {
            foreach (SpriteRenderer sprite in fore_sprites)
            {
                if (sprite.gameObject.tag == "Water")           //water is a special case since we want it to keep its opacity
                    sprite.color = new Color(1.0f, 1.0f, 1.0f, 0.742f);
                else
                    sprite.color = new Color(1.0f, 1.0f, 1.0f);
            }
            isCursed = false;
        }
        else if (!isCursed) //If not cursed, become cursed and alter all of the colors of the foreground
        {
            foreach (SpriteRenderer sprite in fore_sprites)
            {
                if (sprite.gameObject.tag == "Lava")
                    sprite.color = new Color(0.796f, 0.361f, 0.361f);
                if (sprite.gameObject.tag == "Water")
                    sprite.color = new Color(0.00f, 0.741f, 0.282f, 0.742f);
                if (sprite.gameObject.tag == "Structure" || sprite.gameObject.tag == "wall" || sprite.gameObject.tag == "ground" || sprite.gameObject.tag == "hazard")
                    sprite.color = new Color(0.008f, 0.341f, 0.620f);

            }
            isCursed = true;
        }
    }
}
